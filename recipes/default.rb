case node.platform.to_sym
when :mac_os_x
  remote_file "/tmp/get-pip.py" do
    source "https://raw.github.com/pypa/pip/master/contrib/get-pip.py"
    action :create_if_missing
  end

  execute "python /tmp/get-pip.py" do
    not_if {::File.exists? "/usr/local/bin/pip"}
  end

when :centos
  package "python-setuptools"

  execute "easy_install pip"

end
